<?php
/**
 * Created by PhpStorm.
 * User: Paul Palladino
 * Date: 8/4/14
 * Time: 11:10 AM
 */

namespace Neo\MongoUser;

use \Neo\Lib\Utils as Utils;

class UserViewModel extends \Neo\F3\ViewModel {

    public function __construct ($entity) {
        $this->dataMapper = new UserViewModelDataMapper();
        parent::__construct($entity);
    }
} 