<?php
/**
 * Created by PhpStorm.
 * User: Paul Palladino
 * Date: 8/4/14
 * Time: 11:10 AM
 */

namespace Neo\MongoUser;

use \Neo\Lib\Utils as Utils;

class UserEntity extends \Neo\Mongo\MongoEntity {

    public
        $_id       = null,
        $firstName = '',
        $lastName  = '',
        $email     = '',
        $password  = '',
        $role  = '';

    public function __construct ($data = null) {
        $this->dataMapper = new UserEntityDataMapper();
        parent::__construct($data);
    }

    public function validate ($db = null) {

        \Valitron\Validator::addRule('uniqueEmail', function($field, $value, array $params) {

            $db = $params[0][0];
            $existingUser = \Neo\MongoUser\UserDal::getByEmail($db, $value);

            if($existingUser === false){
                // Email is unique
                return true;
            }

            // === would resolve as false, diff objects
            if($existingUser->_id == $this->_id) {
                // Same user account
                return true;
            }
            return false;

        }, 'in use');

        $v = new \Valitron\Validator((array)$this);

        $v->rule('required', ['firstName', 'lastName', 'email', 'password', 'role']);
        $v->rule('email', 'email');
        $v->rule('lengthMin', 'password', 8);
        $v->rule('uniqueEmail', ['email'], [$db]);

        $valid = $v->validate();
        $this->errors = $valid ? array() : $v->errors();
        return $valid;
    }
} 