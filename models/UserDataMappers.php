<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 8/29/14
 * Time: 7:58 AM
 */

namespace Neo\MongoUser;

use \Neo\Lib\Utils as Utils;

class UserEntityDataMapper {
    public function map (&$target, $source) {
        Utils\Mapper::mapValues(
            $target,
            $source,
            array(
                '_id'       => Utils\Mapper::TYPE_MONGO_ID,
                'firstName' => Utils\Mapper::TYPE_STRING,
                'lastName'  => Utils\Mapper::TYPE_STRING,
                'email'     => Utils\Mapper::TYPE_STRING_TO_LOWER,
                'password'  => Utils\Mapper::TYPE_STRING,
                'role'      => Utils\Mapper::TYPE_STRING
            )
        );
    }
}

class UserInsertDataMapper {
    public function map (&$target, $source) {
        Utils\Mapper::mapValues(
            $target,
            $source,
            array(
                '_id'       => Utils\Mapper::TYPE_MONGO_ID,
                'firstName' => Utils\Mapper::TYPE_STRING,
                'lastName'  => Utils\Mapper::TYPE_STRING,
                'email'     => Utils\Mapper::TYPE_STRING_TO_LOWER,
                'password'  => Utils\Mapper::TYPE_STRING,
                'role'      => Utils\Mapper::TYPE_STRING
            )
        );
    }
}

class UserUpdateDataMapper {
    public function map (&$target, $source) {
        Utils\Mapper::mapValues(
            $target,
            $source,
            array(
                'firstName' => Utils\Mapper::TYPE_STRING,
                'lastName'  => Utils\Mapper::TYPE_STRING,
                'email'     => Utils\Mapper::TYPE_STRING_TO_LOWER,
                'password'  => Utils\Mapper::TYPE_STRING,
                'role'      => Utils\Mapper::TYPE_STRING
            )
        );
    }
}

class UserViewModelDataMapper {
    public function map (&$target, $source) {
        Utils\Mapper::mapValues(
            $target,
            $source,
            array(
                '_id'       => Utils\Mapper::TYPE_MONGO_ID_AS_STRING,
                'firstName' => Utils\Mapper::TYPE_STRING,
                'lastName'  => Utils\Mapper::TYPE_STRING,
                'email'     => Utils\Mapper::TYPE_STRING_TO_LOWER,
                'role'      => Utils\Mapper::TYPE_STRING,
                'errors'    => Utils\Mapper::TYPE_DO_NOT_CAST
            )
        );
    }
}

class UserPostDataMapper {
    public function map (&$target, $source) {
        Utils\Mapper::mapValues(
            $target,
            $source,
            array(
                'firstName' => Utils\Mapper::TYPE_STRING,
                'lastName'  => Utils\Mapper::TYPE_STRING,
                'email'     => Utils\Mapper::TYPE_STRING_TO_LOWER,
                'role'      => Utils\Mapper::TYPE_STRING
            )
        );

        if(isset($source->password) && !is_null($source->password)) {
            $target->password = \Neo\Cms\Lib\Auth::encryptPassword($source->password);
        }
    }


}