<?php

/**
 * Created by PhpStorm.
 * User: Paul Palladino
 * Date: 8/4/14
 * Time: 11:10 AM
 */

namespace Neo\MongoUser;

use \Neo\Lib\Utils as Utils;

class UserDal extends \Neo\Mongo\MongoDal {

    protected static
        $collectionName,
        $entityClass,
        $insertMapper,
        $updateMapper;

    public static function getByEmail (&$db, $email) {
        $collection = static::$collectionName;
        $document = $db->$collection->findOne(array('email'=>$email));
        if (is_null($document)) { return false; }
        return static::getEntity($document);
    }

    public static function getByEmailAndPassword (&$db, $email, $password) {
        $collection = static::$collectionName;
        $document = $db->$collection->findOne(array('email'=>$email, 'password'=>$password));
        if (is_null($document)) { return false; }
        return static::getEntity($document);
    }

    public static function staticInit () {
        static::$collectionName = 'cms_users';
        static::$entityClass    = '\Neo\MongoUser\UserEntity';
        static::$insertMapper = new UserInsertDataMapper();
        static::$updateMapper = new UserUpdateDataMapper();
    }
}

UserDal::staticInit();