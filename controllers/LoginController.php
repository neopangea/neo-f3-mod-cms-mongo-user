<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/11/14
 * Time: 12:58 PM
 */

namespace Neo\MongoUser;

class LoginController extends \Neo\F3\Controller {

    public function index( $f3, $args ) {
        parent::__construct( $f3 );
        $template = \Template::instance();
        echo $template->render('vendor/neopangea/f3-mod-cms-mongo-user/templates/login-index.htm');
    }

    public function login( $f3, $args ) {
        parent::__construct( $f3 );

        $response = new \Neo\F3\Response();
        $post     = json_decode($f3->get('BODY'));

        try {
            if(!isset($post->email)) {
                throw new \Exception('email missing');
            }

            if(!isset($post->password)) {
                throw new \Exception('password missing');
            }

            if (! \Neo\Cms\Lib\Auth::loginByEmailAndPassword($post->email, $post->password)) {
                throw new \Exception('credential match not found');
            }

            // If here, ok!
            $response->data->redirectUrl = $this->f3->get('NEO_CMS_LOGIN_REDIRECT_URL');

        } catch (\Exception $e) {
            $response->successful = false;
            $response->message = $e->getMessage();
        }

        exit(json_encode($response));
    }

    public function logout( $f3, $args ) {
        parent::__construct( $f3 );
        \Neo\Cms\Lib\Auth::logout();
    }
}