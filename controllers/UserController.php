<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/11/14
 * Time: 12:58 PM
 */

namespace Neo\MongoUser;

use \Neo\F3        as Neo;

class UserController extends \Neo\F3\Controller {

    public function index( $f3, $args ) {
        parent::__construct( $f3 );
        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_REDIRECT);
        $template = \Template::instance();
        echo $template->render('vendor/neopangea/f3-mod-cms-mongo-user/templates/user-index.htm');
    }

    public function get( $f3, $args ) {
        parent::__construct( $f3 );
        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);
        $response = new \Neo\F3\Response();
        $db       = UserDal::getDB($f3);
        if (isset($_GET['_id'])) {
            $userEntity = UserDal::get($db, $_GET['_id']);
            $userViewModel = new UserViewModel($userEntity);
            $response->data->user = $userViewModel;
        } else {
            $userEntities = UserDal::getAll($db);
            $userViewModels = UserViewModel::getViewModels($userEntities);
            $response->data->users = $userViewModels;
        }

        exit(json_encode($response));
    }

    public function post( $f3, $args ) {
        parent::__construct( $f3 );
        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);
        $response = new \Neo\F3\Response();
        $post     = json_decode($f3->get('BODY'));
        $db       = UserDal::getDB($f3);
        $entity   = new UserEntity();

        if (isset($post->_id)) {
            $entity = UserDal::get($db, $post->_id);
            if (is_null($entity)) { $f3->error('403', 'missing record');}
        }

        $mapper = new UserPostDataMapper();
        $mapper->map($entity, $post);

        if (!$entity->validate($db)) {
            $response->successful = false;
        } else {
            if (!UserDal::save($db, $entity)) { $f3->error('500', 'unable to save'); }
        }
        $response->data->user = new UserViewModel($entity);

        exit(json_encode($response));
    }

    public function install ($f3, $args) {
        parent::__construct( $f3 );

        $default_password = 'neocms123';

        $seed_users = array(
            array(
                'email'     => 'admin@mail.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword('admin'),
                'firstName' => 'Joe',
                'lastName'  => 'Doe',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'john@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'John',
                'lastName'  => 'Harris',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'phil@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'Phil',
                'lastName'  => 'Krick',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'brett@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'Brett',
                'lastName'  => 'Bagenstose',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'damon@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'Damon',
                'lastName'  => 'Williams',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'shane@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'Shane',
                'lastName'  => 'Hoffa',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'aaron@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'Aaron',
                'lastName'  => 'Beaucher',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'bob@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'Bob',
                'lastName'  => 'Trate',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'charles@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'Charles',
                'lastName'  => 'Rowland',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'tim@neo-pangea.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword($default_password),
                'firstName' => 'Tim',
                'lastName'  => 'Corr',
                'role'      => 'admin'
            ),
            array(
                'email'     => 'editor@mail.com',
                'password'  => \Neo\Cms\Lib\Auth::encryptPassword('editor'),
                'firstName' => 'Info',
                'lastName'  => 'Info',
                'role'      => 'editor'
            )
        );

        $db     = UserDal::getDB($f3);

        foreach($seed_users as $seed_user){
            $existing = UserDal::getByEmail($db, $seed_user['email']);
            if(!$existing){
                $newUser = new UserEntity($seed_user);
                if (!UserDal::save($db, $newUser)) {
                    exit('Unable to save');
                }
            } else {
                echo "Already installed: ". $seed_user['email'] ."<br />";
            }

        }

        exit('admin@mail.com/admin');

    }

    public function delete( $f3, $args ) {

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);

        if (!isset($args['id'])) {
            $f3->error('403', 'missing _id');
        }

        $response = new Neo\Response();

        try {
            $db = UserDal::getDB($f3);
            UserDal::delete($db, $args['id']);
        } catch (\Exception $e) {
            $response->successful = false;
            $response->message = $e->getMessage();
        }

        exit(json_encode($response));
    }

} 