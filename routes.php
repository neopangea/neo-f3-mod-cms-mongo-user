<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/20/14
 * Time: 6:39 PM
 */

$app = Neo\F3\App::instance();

// File Manager
$app->f3->route( 'GET    /'.  $app->f3->get('NEO_CMS_SLUG') .'/users/install'     , 'Neo\MongoUser\UserController->install');
$app->f3->route( 'GET    /'.  $app->f3->get('NEO_CMS_SLUG') .'/users'             , 'Neo\MongoUser\UserController->index');
$app->f3->route( 'GET    /'.  $app->f3->get('NEO_CMS_SLUG') .'/services/users'    , 'Neo\MongoUser\UserController->get');
$app->f3->route( 'POST   /'.  $app->f3->get('NEO_CMS_SLUG') .'/services/users'    , 'Neo\MongoUser\UserController->post');
$app->f3->route( 'DELETE /'.  $app->f3->get('NEO_CMS_SLUG') .'/services/users/@id', 'Neo\MongoUser\UserController->delete');

$app->f3->route( 'GET    /'.  $app->f3->get('NEO_CMS_SLUG') .'/login'             , 'Neo\MongoUser\LoginController->index');
$app->f3->route( 'POST   /'.  $app->f3->get('NEO_CMS_SLUG') .'/services/login'    , 'Neo\MongoUser\LoginController->login');

$app->f3->route( 'GET    /'.  $app->f3->get('NEO_CMS_SLUG') .'/logout'            , 'Neo\MongoUser\LoginController->logout');