<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/19/14
 * Time: 5:49 PM
 */

$app = \Neo\F3\App::instance();
\Neo\Cms\Lib\Auth::registerBehavior(new \Neo\MongoUser\Lib\AuthBehavior($app->f3));