/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global angular, NP*/

(function () {

    'use strict';

    angular.module('userManagementApp').factory(
        'userFactory',
        [
            'config',
            '$resource',
            function (config, $resource) {
                return $resource('/'+ config.cmsSlug +'/services/users/:id');
            }
        ]
    );
}());

