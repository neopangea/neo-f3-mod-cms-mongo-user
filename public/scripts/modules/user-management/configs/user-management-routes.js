/*jslint nomen: true, white: true */
/*global angular*/

(function () {

    'use strict';

    angular.module('userManagementApp').config([
        'config',
        '$routeProvider',
        function (config, $routeProvider) {


            var cmsDataResolve = {
                cmsData: ['userFactory', '$location', '$rootScope',
                    function(userFactory, $location, $rootScope) {
                        return userFactory.get({}, function(res) {
                            $rootScope.data = res.data;
                            config.data = res.data;

                            $rootScope.$emit('dataReady', res.data);

                        }, function(err){
                            //console.debug(err);
                            if(err.status == 401){
                                $location.path('/');
                            }
                        }).$promise;
                    }
                ]
            };

            $routeProvider
                .when(
                '/', {
                    redirectTo: '/list'
                })
                .when('/list', {
                    controller: 'userIndexController',
                    resolve: cmsDataResolve,
                    templateUrl: '/vendor/neopangea/f3-mod-cms-mongo-user/public/scripts/modules/user-management/views/user-index-view.html'
                })
                .when('/add', {
                    controller: 'userEditController',
                    resolve: cmsDataResolve,
                    templateUrl: '/vendor/neopangea/f3-mod-cms-mongo-user/public/scripts/modules/user-management/views/user-edit-view.html'
                })
                .when('/edit/:_id', {
                    controller: 'userEditController',
                    resolve: cmsDataResolve,
                    templateUrl: '/vendor/neopangea/f3-mod-cms-mongo-user/public/scripts/modules/user-management/views/user-edit-view.html'
                });
        }
    ]);

}());