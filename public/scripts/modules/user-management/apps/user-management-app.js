/*global angular*/

(function () {
    'use strict';

    angular.module('userManagementApp', [
        'neoCmsBaseModule',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ]);

}());
