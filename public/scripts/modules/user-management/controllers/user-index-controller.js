/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global angular, window, _, alert*/

(function () {

    'use strict';

    angular.module('userManagementApp').controller(
        'userIndexController',
        [
            '$scope',
            '$routeParams',
            'userFactory',
            'neoEnumHelper',
            'cmsData',
            function ($scope, $routeParams, userFactory, neoEnumHelper, cmsData) {

                $scope.data = cmsData.data;

                $scope.ui    = {};

                $scope.state = {
                    options: ['default', 'fetching', 'deleting', 'error'],
                    current: 'default',
                    msg: '',
                    set: function (state, msg) {
                        if ($scope.state.options.indexOf(state) === -1) {
                            console.warn('unknown state: ', state);
                        }
                        $scope.state.msg = msg || '';
                        $scope.state.current = state;
                    }
                };

                $scope.roles = { admin: 'Admin', editor: 'Editor' };


                $scope.delete = function (user) {
                    deleteUser(user);
                };

                function deleteUser(model){

                    $scope.state.set('deleting');

                    userFactory.delete(
                        {id: model._id},
                        function (response) {

                            if (!response.successful) {
                                $scope.state.set('error', response.message || 'check the js console for details');
                                window.console.warn('Error', response);
                                return;
                            }

                            $scope.data.users = _.without($scope.data.users, _.findWhere($scope.data.users, {_id: model._id}));

                            $scope.state.set('default');

                        }, function (error) {
                            $scope.state.set('error', error.status + '-' + error.statusText);
                            window.console.warn('Error:', error);
                        }
                    );
                }


            }
        ]
    );
}());

