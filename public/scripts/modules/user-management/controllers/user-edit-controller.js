/**
 * Created by Paul on 9/12/14.
 */

/*jslint nomen: true, white: true */
/*global angular, window, _*/

(function () {

    'use strict';

    angular.module('userManagementApp').controller(
        'userEditController',
        [
            '$scope',
            '$routeParams',
            'userFactory',
            function ($scope, $routeParams, userFactory) {


                $scope.data  = {
                    user : {
                        firstName : '',
                        lastName  : '',
                        email     : ''
                    }
                };

                $scope.ui    = {};

                $scope.ui.state = {
                    options: {
                        fetching : 'fetching', editing : 'editing', saving : 'saving', saved : 'saved', error : 'error'
                    },
                    current : 'editing',
                    set : function (state) {
                        this.current = state;
                    }
                };


                $scope.roles = { admin: 'Admin', editor: 'Editor' };

                $scope.save = function () {
                    $scope.ui.state.set($scope.ui.state.options.saving);

                    userFactory.save(
                        $scope.data.user,
                        function (response) {
                            if (response.successful) {
                                $scope.ui.state.set($scope.ui.state.options.saved);
                            } else {
                                $scope.ui.state.set($scope.ui.state.options.error);
                                window.console.log('Response error: ', response);
                            }
                            $scope.data = response.data;
                        }
                    );
                };

                /*
                $scope.getEpisodes = function (id) {
                    episodesFactory.get(
                        {id : id},
                        function (response) {
                            if (!response || !response.successful) {
                                alert('Error! Check JS console for details.');
                                console.log('Error: ', response);
                            }
                            $scope.data.episode          = response.data.episode;
                            $scope.data.sections         = response.data.sections;
                            $scope.data.contents         = response.data.contents;
                            $scope.data.contentNodeTypes = response.data.contentNodeTypes;
                        }
                    );
                };
                */

                /**
                 * Initialize
                 */

                if ($routeParams._id) {
                    $scope.ui.state.set($scope.ui.state.options.fetching);
                    userFactory.get(
                        {_id: $routeParams._id},
                        function (response) {
                            if (response.successful) {
                                $scope.ui.state.set($scope.ui.state.options.editing);
                            } else {
                                $scope.ui.state.set($scope.ui.state.options.error);
                                window.console.log('Response error: ', response);
                            }
                            $scope.data = response.data;
                        }
                    );
                }
            }
        ]
    );
}());

