/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global angular, window, _, alert*/

(function () {

    'use strict';

    angular.module('loginApp').controller(
        'loginController',
        [
            '$scope',
            'loginFactory',
            function ($scope, loginFactory) {


                $scope.ui    = {};

                $scope.state = {
                    options: ['default', 'submitting', 'error', 'logged_in'],
                    current: 'default',
                    msg: '',
                    set: function (state, msg) {
                        if ($scope.state.options.indexOf(state) === -1) {
                            console.warn('unknown state: ', state);
                        }
                        $scope.state.msg = msg || '';
                        $scope.state.current = state;
                    }
                };

                $scope.data  = {
                    email: '',
                    password: ''
                };

                $scope.submit = function () {
                    $scope.state.set('submitting');

                    loginFactory.save({
                            email: $scope.data.email,
                            password: $scope.data.password
                        },
                        function (response) {
                            if (!response.successful) {
                                $scope.state.set('error', response.message || 'Error! Check JS console for details.');
                                console.log('Error: ', response);
                                return;
                            }
                            $scope.state.set('logged_in', response.message || 'Logged in!');

                            if (response.data.redirectUrl) {
                                window.location.href = response.data.redirectUrl;
                            }

                        }, function (error) {
                            $scope.state.set('error', 'Error! Check JS console for details.');
                            console.log('Error: ', error);
                        }
                    );

                };
            }
        ]
    );
}());

