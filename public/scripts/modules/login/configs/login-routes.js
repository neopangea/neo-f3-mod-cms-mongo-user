/*jslint nomen: true, white: true */
/*global angular*/

(function () {

    'use strict';

    angular.module('loginApp').config([
        '$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/login', {
                    controller: 'loginController',
                    templateUrl: '/vendor/neopangea/f3-mod-cms-mongo-user/public/scripts/modules/login/views/login-view.html'
                })
                .otherwise({
                    redirectTo: '/login'
                });
        }
    ]);

}());