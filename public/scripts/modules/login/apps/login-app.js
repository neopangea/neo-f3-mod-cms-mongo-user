/*global angular*/

(function () {
    'use strict';

    angular.module('loginApp', [
        'neoCmsBaseModule',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ]);

}());
