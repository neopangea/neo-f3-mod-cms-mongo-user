<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/19/14
 * Time: 5:46 PM
 */

namespace Neo\MongoUser\Lib;


class AuthBehavior extends \Neo\Cms\Lib\AuthBehavior {

    const
        LOGGED_IN_USER_ID = 'LOGGED_IN_USER_ID',
        NEO_CMS = 'NEO_CMS';

    protected
        $loggedInUser,
        $f3;

    public function __construct ($f3) {

        $this->f3 = $f3;
        $userId = $this->getLoggedInUserId();
        if ($userId !== false) {
            $this->loginByUserId(new \MongoId($userId));
        }

    }

    /**
     * Attempts to log in user via user _id. If fails, sets user to null. Always returns bool.
     * @param  \MongoId $userId
     * @return bool
     */

    public function loginByUserId ($userId) {

        $db         = \Neo\Mongo\MongoDal::getDB($this->f3);
        $userEntity = \Neo\MongoUser\UserDal::get($db, $userId);

        if ($userEntity !== false) {
            $this->setLoggedInUser($userEntity);
        } else {
            $this->setLoggedInUser(null);
        }
        return ! is_null($this->loggedInUser);

    }

    /**
     * Attempts to log in user via email & password pair. If fails, sets user to null. Always returns bool.
     * @param $userId
     * @return bool
     */

    public function loginByEmailAndPassword ($email, $password) {
        $db         = \Neo\Mongo\MongoDal::getDB($this->f3);
        $userEntity = \Neo\MongoUser\UserDal::getByEmailAndPassword(
            $db,
            strtolower($email),
            \Neo\Cms\Lib\Auth::encryptPassword($password)
        );
        if ($userEntity !== false) {
            $this->setLoggedInUser($userEntity);
        } else {
            $this->setLoggedInUser(null);
        }
        return ! is_null($this->loggedInUser);
    }

    public function authGate($role = 'user', $behavior = \Neo\Cms\Lib\Auth::GATE_REDIRECT){

        if (!$this->isLoggedIn()) {
            switch ($behavior) {
                case \Neo\Cms\Lib\Auth::GATE_REDIRECT:
                    $this->f3->reroute($this->f3->get('NEO_CMS_LOGIN_URL'));
                    break;
                case \Neo\Cms\Lib\Auth::GATE_BOOL:
                    return false;
                    break;
                case \Neo\Cms\Lib\Auth::GATE_THROW_401:
                    $this->f3->error('401', 'unauthorized');
            }
        }

        // the user is logged in at this point

        //get the user's MongoId
        $userId = $this->getLoggedInUserId();
        $db         = \Neo\Mongo\MongoDal::getDB($this->f3);
        $userEntity = \Neo\MongoUser\UserDal::get($db, $userId);

        // get the user's assigned role from the userEntity
        $role = $userEntity->role;

        // set the USER and ROLE configuration variables
        $this->f3->set('USER', $userId);

        $this->f3->set('ROLE', $role);

        switch($role){
            case 'admin':
            case 'editor':
            case 'user':
            default:
                return true;
                break;
        }

    }

    public function userGate($behavior = \Neo\Cms\Lib\Auth::GATE_REDIRECT) {

        return $this->authGate('user', $behavior);

    }

    public function adminGate($behavior = \Neo\Cms\Lib\Auth::GATE_REDIRECT) {

        return $this->authGate('admin', $behavior);

    }

    public function isLoggedIn() {
        return ! is_null($this->loggedInUser);
    }

    protected function getLoggedInUserId () {

        // \Neo\Lib\Utils\SessionManager::sessionStart(AuthBehavior::NEO_CMS);
        session_start();
        $userId = isset($_SESSION[AuthBehavior::LOGGED_IN_USER_ID]) ? $_SESSION[AuthBehavior::LOGGED_IN_USER_ID] : false;
        session_write_close();

        // \Neo\Lib\Utils\SessionManager::sessionClose();
        return $userId;
    }

    /**
     * To unset logged in user, pass in null for $userEntity
     * @param $userEntity
     */

    protected function setLoggedInUser ($userEntity) {
        $this->loggedInUser = $userEntity;
        // \Neo\Lib\Utils\SessionManager::sessionStart(AuthBehavior::NEO_CMS);
        session_start();
        $_SESSION[AuthBehavior::LOGGED_IN_USER_ID] = is_null($userEntity) ? null : $userEntity->_id->{'$id'};
        session_write_close();
        // \Neo\Lib\Utils\SessionManager::sessionClose();
    }

    public function logout () {
        $this->setLoggedInUser(null);
        $this->f3->reroute($this->f3->get('NEO_CMS_LOGIN_URL'));
    }

    public function encryptPassword ($password) {
        return password_hash($password, PASSWORD_DEFAULT, array('salt'=>'HU9[S,v2 D1\BtzPPP#fu*~dGdG'));
    }
} 